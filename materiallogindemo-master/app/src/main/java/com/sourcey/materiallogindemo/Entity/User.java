package com.sourcey.materiallogindemo.Entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;

@Entity(tableName = "User")
public class User {
    @ColumnInfo(name = "email")
    private String email;

    @ColumnInfo (name = "password")
    private  String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
