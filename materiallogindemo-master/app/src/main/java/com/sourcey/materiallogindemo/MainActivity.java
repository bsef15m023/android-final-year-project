package com.sourcey.materiallogindemo;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.sourcey.materiallogindemo.Entity.Movie;
import com.sourcey.materiallogindemo.Entity.MoviesAdapter;
import com.sourcey.materiallogindemo.Entity.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private AppBarLayout appBarLayout;
    private ViewPager viewPager;
    Context c=this;
    Bundle extras;
    private String newLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tabLayout=(TabLayout)findViewById(R.id.tablayout_id);
        appBarLayout=(AppBarLayout)findViewById(R.id.appbarid);
        viewPager=(ViewPager)findViewById(R.id.viewpager_id);
        ViewPageAdapter adapter=new ViewPageAdapter(getSupportFragmentManager());

        extras = getIntent().getExtras();

        //Adding Multiple Fragments
        adapter.AddFragment(new FragmentTwo(this ),"Library");

        adapter.AddFragment(new FragmentTwo(this ),"Entry");

        adapter.AddFragment(new FragmentTwo(this ),"Loby");

       // adapter.AddFragment(new FragmentTwo(this ),"Parking");
        //adapter.AddFragment(new FragmentTwo(this ),"Faculty");
        adapter.AddFragment(new FragmentTwo(this ),"Visiting");

        if(extras!=null)
        {
            newLocation=extras.getString("Location_Name");

            if(!newLocation.equals(""))
            {
                adapter.AddFragment(new FragmentTwo(this ),newLocation);

            }
        }

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

    }

}
