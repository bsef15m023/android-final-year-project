package com.sourcey.materiallogindemo.Entity;

/**
 * Created by tkxel on 4/3/18.
 */
public class Movie {
    private String title;


    public Movie() {
    }

    public Movie(String title) {
        this.title = title;
    }



    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }


}
