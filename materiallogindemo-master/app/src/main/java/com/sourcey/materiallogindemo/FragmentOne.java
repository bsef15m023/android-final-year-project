package com.sourcey.materiallogindemo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import java.util.HashMap;
import java.util.Map;

public class  FragmentOne extends Fragment
        implements MediaPlayer.OnPreparedListener,
        SurfaceHolder.Callback {
    final static String USERNAME = "admin";
    final static String PASSWORD = "admin";
    final static String RTSP_URL = "rtsp://admin:admin@192.168.1.109:554/cam/realmonitor?channel=1&subtype=0";

    private MediaPlayer _mediaPlayer;
    private SurfaceHolder _surfaceHolder;
    Context mcontext;
    View view;

    public FragmentOne() {

    }

    @SuppressLint("ValidFragment")
    public FragmentOne(Context mainContext) {


        mcontext = mainContext;
    }

    @Nullable
    @Override

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.one_fragment, container, false);
        Context context = container.getContext();
        // Set up a full-screen black window.
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
       /// Window window = getWindow();
        //window.setFlags(
        //WindowManager.LayoutParams.FLAG_FULLSCREEN,
          //WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //window.setBackgroundDrawableResource(android.R.color.black);


        // Configure the view that renders live video.
        SurfaceView surfaceView =
                view.findViewById(R.id.surfaceView);
        _surfaceHolder = surfaceView.getHolder();
        _surfaceHolder.addCallback(this);
        _surfaceHolder.setFixedSize(320, 240);
        return view;
    }





    /*
SurfaceHolder.Callback
*/

    @Override
    public void surfaceChanged(
            SurfaceHolder sh, int f, int w, int h) {}

    @Override
    public void surfaceCreated(SurfaceHolder sh) {
        _mediaPlayer = new MediaPlayer();
        _mediaPlayer.setDisplay(_surfaceHolder);

        Context context = mcontext;
        Map<String, String> headers = getRtspHeaders();
        Uri source = Uri.parse(RTSP_URL);

        try {
            // Specify the IP camera's URL and auth headers.
            _mediaPlayer.setDataSource(context, source, headers);

            // Begin the process of setting up a video stream.
            _mediaPlayer.setOnPreparedListener(this);
            _mediaPlayer.prepareAsync();
        }
        catch (Exception e) {}
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder sh) {
        _mediaPlayer.release();
    }

    private Map<String, String> getRtspHeaders() {
        Map<String, String> headers = new HashMap<String, String>();
        String basicAuthValue = getBasicAuthValue(USERNAME, PASSWORD);
        headers.put("Authorization", basicAuthValue);
        return headers;
    }

    private String getBasicAuthValue(String usr, String pwd) {
        String credentials = usr + ":" + pwd;
        int flags = Base64.URL_SAFE | Base64.NO_WRAP;
        byte[] bytes = credentials.getBytes();
        return "Basic " + Base64.encodeToString(bytes, flags);
    }
    public void onPrepared(MediaPlayer mp) {
        _mediaPlayer.start();
    }
}
