package com.sourcey.materiallogindemo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.sourcey.materiallogindemo.Entity.Movie;
import com.sourcey.materiallogindemo.Entity.MoviesAdapter;
import com.sourcey.materiallogindemo.Entity.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class  FragmentTwo extends Fragment
         {
    final static String USERNAME = "fracker";
    final static String PASSWORD = "fracker";
    final static String RTSP_URL = "rtsp://fracker:fracker@192.168.1.108:554/cam/realmonitor?channel=1&subtype=0";

    private MediaPlayer _mediaPlayer;
    private SurfaceHolder _surfaceHolder;
    Context mcontext;
    View view;
    private List<Movie> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MoviesAdapter mAdapter;

    public FragmentTwo() {

    }

    @SuppressLint("ValidFragment")
    public FragmentTwo(Context mainContext) {


        mcontext = mainContext;
    }

    @Nullable
    @Override

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.two_fragment, container, false);
        Context context = container.getContext();
        //Set up a full-screen black window.
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Window window = getWindow();
        //window.setFlags(
        //WindowManager.LayoutParams.FLAG_FULLSCREEN,
          // WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //window.setBackgroundDrawableResource(android.R.color.black);


        // Configure the view that renders live video.
        /*SurfaceView surfaceView =
                view.findViewById(R.id.surfaceView);
        _surfaceHolder = surfaceView.getHolder();
        _surfaceHolder.addCallback(this);
        _surfaceHolder.setFixedSize(320, 240);*/


        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mAdapter = new MoviesAdapter(movieList);
        recyclerView.setHasFixedSize(true);

             // vertical RecyclerView
             // keep movie_list_row.xml width to `match_parent`
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mcontext);

             // horizontal RecyclerView
             // keep movie_list_row.xml width to `wrap_content`
             //RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);

        recyclerView.setLayoutManager(mLayoutManager);

             // adding inbuilt divider line
        recyclerView.addItemDecoration(new DividerItemDecoration(mcontext, LinearLayoutManager.VERTICAL));

             // adding custom divider line with padding 16dp
             // recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.HORIZONTAL, 16));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);

             // row click listener
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(mcontext, recyclerView, new RecyclerTouchListener.ClickListener() {
             @Override
             public void onClick(View view, int position) {
                 Movie movie = movieList.get(position);
                 Toast.makeText(mcontext, movie.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();
             }

             @Override
             public void onLongClick(View view, int position) {

             }
         }));

             prepareSampleMovieData();
            return view;
         }



    private void prepareSampleMovieData() {

        movieList.clear();
        Movie movie = new Movie("Camera0");
        movieList.add(movie);

        movie = new Movie("Camera1");
        movieList.add(movie);

        // notify adapter about data set changes
        // so that it will render the list with new data
        mAdapter.notifyDataSetChanged();

    }
}

