package com.example.hmtahiraziz.securify;

import
        android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by H TAHIR AZIZ on 3/2/2019.
 */

public class WarningAdapter extends RecyclerView.Adapter<WarningAdapter.MyViewHolder>{
    private List<War> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView image;
        public TextView enterTime;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            image = (ImageView) view.findViewById(R.id.image);
            enterTime = (TextView) view.findViewById(R.id.genre);
        }
    }

    public WarningAdapter(List<War> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public WarningAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.warning_list_row, parent, false);

        return new WarningAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(WarningAdapter.MyViewHolder holder, int position) {

        War movie = moviesList.get(position);

        holder.title.setText(movie.getTxt());
        holder.image.setImageResource(movie.getImageId());
        holder.enterTime.setText(movie.getEnter_time());
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
