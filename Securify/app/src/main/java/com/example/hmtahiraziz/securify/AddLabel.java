package com.example.hmtahiraziz.securify;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AddLabel extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_label);
        final TextView person=(TextView)findViewById(R.id.input_email);
        Button button=(Button)findViewById(R.id.btn_login);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent i = new Intent(AddLabel.this, Analytics.class);
                i.putExtra("new",person.toString());
                startActivity(i);
            }
        });

    }
}
