package com.example.hmtahiraziz.securify;

/**
 * Created by H TAHIR AZIZ on 3/2/2019.
 */

public class War {
    public int imageId;
    public String Enter_time;
    public String Txt;


    public War(String enter_time) {
        Enter_time = enter_time;
    }

    public War(int imageId, String enter_time, String txt) {
        this.imageId = imageId;
        Enter_time = enter_time;
        Txt = txt;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getEnter_time() {
        return Enter_time;
    }

    public void setEnter_time(String enter_time) {
        Enter_time = enter_time;
    }

    public String getTxt() {
        return Txt;
    }

    public void setTxt(String txt) {
        Txt = txt;
    }
}
