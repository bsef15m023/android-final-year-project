package com.example.hmtahiraziz.securify;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Warning extends AppCompatActivity {
    private List<War> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private WarningAdapter mAdapter;

    private ImageView unknown_Detected;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_warning);
        /*unknown_Detected=(ImageView) findViewById(R.id.unknown_person_1);
        unknown_Detected.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialogBox();

            }
        });*/

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new WarningAdapter(movieList);

        recyclerView.setHasFixedSize(true);

        // vertical RecyclerView
        // keep movie_list_row.xml width to `match_parent`
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

        // horizontal RecyclerView
        // keep movie_list_row.xml width to `wrap_content`
        //RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);

        recyclerView.setLayoutManager(mLayoutManager);

        // adding inbuilt divider line
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        // adding custom divider line with padding 16dp
        // recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.HORIZONTAL, 16));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);

        // row click listener
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                dialogBox();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        prepareSampleWarningData();


    }
    public void dialogBox() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("An unknown person entered");
        alertDialogBuilder.setPositiveButton("TakeAction",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent i=new Intent(Warning.this, AddLabel.class);
                        startActivity(i);
                    }
                });

        alertDialogBuilder.setNegativeButton("Dismiss",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {


                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    private void prepareSampleWarningData() {
        War movie = new War(R.drawable.unkown1,"Enter_Time      2:15am","Unknown Person Detected");
        movieList.add(movie);

        movie = new War(R.drawable.person1,"Enter_Time      3:15am","Unknown Person Detected");
        movieList.add(movie);

        movie = new War(R.drawable.unkown1,"Enter_Time      6:15am","Unknown Person Detected");
        movieList.add(movie);

        movie = new War(R.drawable.person1,"Enter_Time      9:15am","Unknown Person Detected");
        movieList.add(movie);

        movie = new War(R.drawable.unkown1,"Enter_Time      5:15am","Unknown Person Detected");
        movieList.add(movie);

        movie = new War(R.drawable.person1,"Enter_Time      1:25am","Unknown Person Detected");
        movieList.add(movie);

        movie = new War(R.drawable.unkown1,"Enter_Time      6:20am","Unknown Person Detected");
        movieList.add(movie);

        movie = new War(R.drawable.person1,"Enter_Time      7:15am","Unknown Person Detected");
        movieList.add(movie);

        movie = new War(R.drawable.unkown1,"Enter_Time      8:15am","Unknown Person Detected");
        movieList.add(movie);

        movie = new War(R.drawable.person1,"Enter_Time      9:15am","Unknown Person Detected");
        movieList.add(movie);

        movie = new War(R.drawable.unkown1,"Enter_Time      11:15am","Unknown Person Detected");
        movieList.add(movie);

        movie = new War(R.drawable.person1,"Enter_Time      12:15am","Unknown Person Detected");
        movieList.add(movie);

        // notify adapter about data set changes
        // so that it will render the list with new data
        mAdapter.notifyDataSetChanged();
    }

}
