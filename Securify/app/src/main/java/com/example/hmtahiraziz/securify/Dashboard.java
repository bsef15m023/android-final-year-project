package com.example.hmtahiraziz.securify;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class Dashboard extends AppCompatActivity {

    ImageView analytics,warning,liveStream;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        analytics=findViewById(R.id.Analytics);
        warning=findViewById(R.id.warning);
        liveStream=findViewById(R.id.live);
        analytics.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i=new Intent(Dashboard.this, MainActivity.class);
                startActivity(i);


            }
        });
        warning.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i=new Intent(Dashboard.this, Warning.class);
                startActivity(i);


            }
        });
        liveStream.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i=new Intent(Dashboard.this, Camera.class);
                startActivity(i);


            }
        });



    }
}
