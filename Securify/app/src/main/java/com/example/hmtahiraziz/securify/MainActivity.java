package com.example.hmtahiraziz.securify;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.hmtahiraziz.securify.Movie;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<Movie> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MoviesAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new MoviesAdapter(movieList);

        recyclerView.setHasFixedSize(true);

        // vertical RecyclerView
        // keep movie_list_row.xml width to `match_parent`
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

        // horizontal RecyclerView
        // keep movie_list_row.xml width to `wrap_content`
        //RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);

        recyclerView.setLayoutManager(mLayoutManager);

        // adding inbuilt divider line
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        // adding custom divider line with padding 16dp
        // recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.HORIZONTAL, 16));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);

        // row click listener
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent i=new Intent(MainActivity.this, Calendar.class);
                startActivity(i);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        prepareSampleMovieData();
    }

    private void prepareSampleMovieData() {

        String  URL="https://hellodemoflask.azurewebsites.net/";
        String name;
        final tasks[] task = new tasks[1];


        StringRequest request=new StringRequest(Request.Method.GET,URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Volley",response);
                Toast.makeText(MainActivity.this, "I got it", Toast.LENGTH_LONG).show();
                GsonBuilder gsonBuilder=new GsonBuilder();
                Gson gson=gsonBuilder.create();
                List<tasks> list= Arrays.asList(gson.fromJson(response,tasks[].class));
                for(int i=0;i<list.size();i++)
                {
                    task[0] =new tasks(list.get(i).getId(),list.get(i).getName(),list.get(i).getQuantity());
                    Toast.makeText(MainActivity.this, task[0].getId()+"   "+ task[0].getName()+"   "+ task[0].getQuantity()+"   ", Toast.LENGTH_LONG).show();

                }




            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(MainActivity.this, "Something Went Wrong", Toast.LENGTH_LONG).show();

            }
        });
        RequestQueue queue= Volley.newRequestQueue(this);
        queue.add(request);



        Movie movie = new Movie("Tahir Aziz", "", "");
        movieList.add(movie);

        movie = new Movie(" Muneeb ul Hassan", "", "");
        movieList.add(movie);

        movie = new Movie("Talha Khan ", "", "");
        movieList.add(movie);


        movie = new Movie(" Misbah Hassan Awan", "", "");
        movieList.add(movie);

        movie = new Movie("Hashir Mehmood", "", "");
        movieList.add(movie);

        movie = new Movie(" Shaheer Aziz", "", "");
        movieList.add(movie);

        movie = new Movie("Hamna Malik ", "", "");
        movieList.add(movie);

        movie = new Movie("Ameer Hamza", "", "");
        movieList.add(movie);

        movie = new Movie(" Ali Raza", "", "");
        movieList.add(movie);

        movie = new Movie("Umar Khan ", "", "");
        movieList.add(movie);


        movie = new Movie("Jack", "", "");
        movieList.add(movie);

        movie = new Movie("Allen", "", "");
        movieList.add(movie);

        movie = new Movie("John Singh", "", "");
        movieList.add(movie);


        // notify adapter about data set changes
        // so that it will render the list with new data
        mAdapter.notifyDataSetChanged();
    }

}

